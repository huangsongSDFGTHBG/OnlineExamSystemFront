// 接口配置
module.exports = {
	devServer: { //设置本地默认端口
		port:8888, //设置vue前端端口
		proxy: { //设置代理，必须填
			'/api': {  //设置拦截器
				target: 'http://127.0.0.1:8000',  //代理的目标地址
				changeOrigin: true,  //是否设置同源，输入是的
				pathRewrite: {  //路径重写
					'^/api': '/'  //选择忽略拦截器里面的内容
				}
			}
		}
	}
}